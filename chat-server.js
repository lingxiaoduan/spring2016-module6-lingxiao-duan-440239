// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");

// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
 
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);

var usernames={};
var owner={}; 
var passwords=[];
var rroom={};
var roomsP={};
var info={};
var rooms={};
var io=socketio.listen(app);


io.sockets.on("connection", function(socket){
socket.on('adduser', function(username){
	socket.username=username;
	usernames[username]=username;
	io.sockets.connected[username]=socket.id;
	socket.room='CommonRoom';
	rroom[username]=socket.room
	socket.join('CommonRoom');
	console.log("new user: "+socket.username);
	io.sockets.emit("currentUsers",usernames);
	socket.emit("currentRooms",'CommonRoom');
	socket.emit("message_to_client",{message: "You are now logged in as: "+socket.username });
	info[socket.username]="";
});

//create public room
socket.on('create', function(newroom) {
	if(rooms[newroom]===newroom){
		socket.emit("message_to_client",{message: newroom+" already exists"});
	}
	else{
	socket.leave(socket.room);
	io.sockets.emit('alert_join', {message: socket.username+" has left "+socket.room});
	socket.join(newroom);
	socket.room=newroom;
	rooms[newroom]=newroom;
	rroom[socket.username]=socket.room;
	owner[socket.room]=socket.username;
	io.sockets.emit("displayRooms",rooms);
	socket.emit("currentRooms",newroom);
	io.sockets.emit('alert_join', {message: socket.username+" has created"+" "+newroom});

	}
});

//join public room
socket.on('joinP', function(newroom) {

		socket.leave(socket.room);
		io.sockets.emit('alert_join', {message: socket.username+" has left"+" "+socket.room});
		socket.join(newroom);
		socket.room=newroom;
		rooms[newroom]=newroom;
		rroom[socket.username]=socket.room;
		io.sockets.emit("displayRooms",rooms);
		socket.emit("currentRooms",newroom);
		io.sockets.emit('alert_join', {message: socket.username+" has joined "+newroom});
});

//create private room
socket.on('privateR', function(roomP, password) {
	if(roomsP[roomP]===roomP){
		socket.emit("message_to_client",{message: roomP+" already exists"});
	}else {
	socket.leave(socket.room);
	io.sockets.emit('alert_join', {message: socket.username+" has left "+socket.room});
	p=password;
	socket.password=password;
	socket.join(roomP);
	socket.room=roomP;
	roomsP[roomP]=roomP;
	rroom[socket.username]=socket.room
	owner[socket.room]=socket.username;
	info[socket.username]+=" "+"room: "+socket.room+" password: "+password;
	passwords.push(password); 
	io.sockets.emit("pRooms",roomsP);
	socket.emit("currentRooms",roomP);
	io.sockets.emit('alert_join', {message: socket.username+" has created"+" "+roomP});
		if (socket.username===owner[socket.room]){
			socket.emit("get_room", info[socket.username]);


		}
	}
});


//join private room
socket.on('joinP', function(roomP, password2) {

		for (var i=0; i<passwords.length; i++){
			if((passwords[i]===password2)){
				socket.leave(socket.room);
				io.sockets.emit('alert_join', {message: socket.username+" has left"+" "+socket.room});
				socket.join(roomP);
				socket.room=roomP;
				roomsP[roomP]=roomP;
				rroom[socket.username]=socket.room
				socket.emit("currentRooms",roomP);
				io.sockets.emit('alert_join', {message: socket.username+" has joined"+" "+roomP});
			}else{
				// socket.emit("message_to_client",{message: "When trying to access"+" "+"<"+roomP+">"+" "+"you put in the wrong password! Try Again!"});
			}
		}
});

//private message
socket.on('whisp', function(userw, messw){
	if(rroom[socket.username]===rroom[userw]){
		socket.emit("message_to_client",{message: "whisper sent to <"+userw+">: "+messw});
		io.to(io.sockets.connected[userw]).emit("message_to_client",{message: "<"+socket.username+"> whispers "+messw}) 
	}
	else{
        socket.emit("message_to_client",{message: "<"+userw+"> not found in your chat room"});
    }
});

//kick
socket.on('kickuser',function(user_kick){
    if (socket.username !==owner[socket.room]) {
        socket.emit("message_to_client",{message: "Only room owner can do that!"});
    }
	else if (rroom[user_kick] !==socket.room) {
    
	//user not in the room
		socket.emit("message_to_client",{message: user_kick+" is not in your room"});
	}
	else{
		io.sockets.emit('Kick',user_kick);

		io.sockets.emit('alert_join', {message: user_kick+" has been kicked from "+socket.room});
		io.to(io.sockets.connected[user_kick]).emit("message_to_client",{message: "you have been removed from the current room!"}) 
		io.to(io.sockets.connected[user_kick]).emit("currentRooms",'CommonRoom');
	}
});

socket.on('kickout', function() {
    socket.leave(socket.room);
    socket.room='CommonRoom';
    rroom[socket.username]=socket.room;
    socket.join('CommonRoom');
});

//remove room
//back
socket.on('back', function(newroom) {

		socket.leave(socket.room);
		io.sockets.emit('alert_join', {message: socket.username+" has left"+" "+socket.room});
		socket.join(newroom);
		socket.room=newroom;
		rooms[newroom]=newroom;
		rroom[socket.username]=socket.room;
		io.sockets.emit("displayRooms",rooms);
		socket.emit("currentRooms",newroom);
		io.sockets.emit('alert_join', {message: socket.username+" has joined "+newroom});
});




socket.on('message_to_server', function(data) {
        // This callback runs when the server receives a new message from the client.
        console.log(socket.username+" "+":"+" "+data["message"]); // log it to the Node.JS output
        io.sockets.to(socket.room).emit("message_to_client",{message: "["+socket.room+"]"+" <"+socket.username+">: "+data["message"] })
});

socket.on('disconnect', function(){
	delete usernames[socket.username];
	io.sockets.emit("currentUsers",usernames);
});
});